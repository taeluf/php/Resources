<?php

namespace Tlf\Resource\Test;

class R extends \Taeluf\Tester {

    public function prepare(){
        \R::setup();
        echo "\nstart prep\n";
        // var_dump(R());
        // exit;
        \R()->set("one","One Thing");
        // exit;
        R()->put(["two"=>'number 2']);
        R()->put(["one"=>"Sub one", 
            "two"=>"Sub two"], "dim.");

        R()->load(__DIR__."/dim-sub.json");

        R()->load(__DIR__.'/defaults.json');

        R()->set("over", "a string");
        R()->set("over.one", "String in an array");  
        R()->set("over.two", "Woah Woah Woah");
        echo "\n\n<br><br>prep done<br><br>\n\n";
    }

    public function testBaseOne(){
        // var_dump(R());
        // exit;
        // var_dump(R("one"));
        $this->compare(R("one"),"One Thing");
    }
    public function testBaseTwo(){
        $this->compare(R("two"),"number 2");
    }
    public function testDimOne(){
        $this->compare(R("dim.one"),"Sub one");
    }
    public function testDimTwo(){
        $this->compare(R("dim.two"),"Sub two");
    }
    public function testFileDimSubArray(){
        // echo "\n";  
        // // var_dump(R());
        // var_dump(R("dim.sub."));
        // echo "\n";
        $this->compare(R("dim.sub."),["one"=>'The first dim-sub',
                                "two"=>'The 2nd dim-sub']);
    }
    public function testListWithDefault(){
        $defaultArray = ["an"=>"cat", "a"=>"dog", "another"=>"mouse"];
        $this->compare(R("not.found.", $defaultArray),$defaultArray);
    }
    public function testFileDimSubNoValue(){
        // var_dump(R());
        // exit;
        $this->compare(R("dim.sub",null),null);
    }
    public function testFileDimSubOne(){
        $this->compare(R("dim.sub.one"),'The first dim-sub');
    }
    public function testFileDimSubTwo(){
        $this->compare(R("dim.sub.two"),'The 2nd dim-sub');
    }
    public function testDefaultsOne(){
        $this->compare(R("defOne","Default 1"),'Default 1');
    }
    public function testDefaultsTwo(){
        $this->compare(R("defTwo","Default 2"),'Default 2');
    }
    public function testDefaultsExisting(){
        // var_dump(R());
        // exit;
        $this->compare(R("defNot","Don't Show This"), 'SHould show this string from defaults.json');
    }
    public function testOverArray(){
        // var_dump(R("over."));
        $this->compare(R("over."), [
            '='=>'a string',
            'one'=> 'String in an array',
            'two'=> 'Woah Woah Woah'
        ]);
    }
    public function testOverBase(){
        $this->compare(R("over"), "a string");
    }
    public function testOverOne(){
        $this->compare(R("over.one"), "String in an array");
    }
    public function testOverTwo(){
        $this->compare(R("over.two"), "Woah Woah Woah");
    }

    public function testCaseSensitivity(){
        $this->compare(R("OnE"), "One Thing");
    }
    public function testPartialKeyList(){
        // var_dump(R("o."));
        $this->compare(R("o.",null), null);
    }

    // public function testCustomFormat(){
    //     R(function($fileContent, $fileExt),
    //         ['yaml'])
    // }
}
