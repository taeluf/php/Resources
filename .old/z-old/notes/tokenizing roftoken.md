I seem to be at a major snag. I'm trying to do something & I can't figure it out. The way different parts relate is confusing as heck & I can't wrap my head around it. When I change one piece, it so significantly changes other pieces in ways that are so not expected.

The format is:

Some.Name: some value
Some.Other.Name: Some
multi
line value
Some.Complex.Name:::: Some
Thingthat.has: multiple lines & things that look like new params
so I need the delimiter
:::
Next.value: heck yeah, everything works now!!!


So there are different ways that I START a key=>value pair:
When a new line begins with /[a-zA-Z\.]*:/ as long as I am not IN another key=>value pair

A key=>value pair ends when:
a \n is encountered, followed by a key=>value pair starter.
or when the current delimiter is encounter on a line of its own
or the file ends

So the following ends the parsing of a key=>value pair:
- \n[a-zA-Z\.]\:...
- \n:::\n
- $

And a key=>value pair starts when:
- [a-zA-Z\.]\:


So how do I structure the parser then? And is a token also the parser? No. I think instead, I will have the parser process tokens.

So it will loop, much as it does now.

But when I'm preg_matching, I need to be able to test against two things:
- The entire (or usually end of) the text that has been loaded in so far
- The beginning (or sometimes all of?) the remaining text to be processed

I need to 
- be able to use the match in the next step, in the 'do'/'end' step
- ensure sequential operation
- be able to pass remaining text back to the parent or to the previous token
- split the current token into multiple other tokens & process them sequentially
- be able to store data globally 
- be able to iterate over all parsed data to get an array & other desired output
- Set a hierarchy for different parts of the data.


So in the case of parsing ROFToken.php, I have a few parts:

Namespace.category.param(type): value and possibly
<p>it wil be</p>
<b>multiline</b>
NewNamespace.category.newParam(array): I am, some array, of several, items
DelimSpace.someParam::: I will have
wildtext: and I want to make sure this doesn't get cut
:and it still will be safe
::
NextSpace.someParam: just a value


So we have the following parts:
- property name
- property type
- property value
- function (":" means equals, here)
- optional delimiter (hard end of paramater)

A paramater ends because of 
- a delimiter
- a new property definition
- end of file

A paramater starts because of
1.) start of file or new line
2.) a matching pattern for paramater start


- I parse a string & when I reach the first property start, I disregard the otherwise processed text.
- I create a new token/parser with the remainder of the text (including the property start portion)
- I process the delimiter to determine what parser I'm using next
    - if delimiter, then EndsWithDelimiter Parser
        - then restart at step 1 with the remaining text
    - else, EndsWithNewParamater parser
        - restart at step 1 with the remaining text (including the new paramater string)


There's the part that ends it, the part that goes into the next token, and the part that remains in this token