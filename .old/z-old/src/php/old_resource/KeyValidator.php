<?php

namespace ROF\Resource;

class KeyValidator {


    static public function isValid($resourceType,$key){
        switch ($resourceType){
            default:
                if (preg_match('/^[a-zA-Z0-9\-\_]*$/',$key)===1){
                    return TRUE;
                } else {
                    return FALSE;
                }
        }
    }

    static public function throwKeyError($lookupKey,$lastPart,$part,$value) {

        ob_start();
            var_dump($value);
        $dumped = ob_get_clean();
        $dumped = substr($dumped,0,-1);
        throw new \Exception(
            "Lookup key '{$lookupKey}' is not valid. The key '{$part}' is not valid. With resource type '{$lastPart}' and var_dump'd value '{$dumped}'"
        );
    }


}

?>