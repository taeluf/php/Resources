<?php

namespace ROF\Resource;

class TypeValidator {


    static public function isValid($resourceType, $value){
        switch ($resourceType){

            case "secure":
            case "string":
                return is_string($value);
            case "error":
                $message = $value['dev'] ?? $value['public'] ?? NULL;
                return ($message!==NULL);
            case "stylesheet":
            case "image":
            case "file":
                $anyValue = $value['url'] ?? $value['path'] ?? $value['raw_data'] ?? NULL;
                if ($anyValue!==NULL){
                    return TRUE;
                } else {
                    return FALSE;
                }
            case "dir":
                if (is_dir($path)){
                    return TRUE;
                } else {
                    return FALSE;
                }
            // case "post":
            // case "get":
            //     return TRUE;

            default:
                return TRUE;
        }
        foreach ($validTypes as $typeChecker){
            $typeCheckerStr = print_r($typeChecker,TRUE);
            if (!is_callable($typeChecker))throw new \Exception("the type checker function '{$typeCheckerStr}' is not valid");
            if (!call_user_func($typeChecker,$value)){
                ob_start();
                    var_dump($value);
                $dumped = ob_get_clean();
                $dumped = substr($dumped,0,-1);
                throw new \Exception("Lookup key '{$lookupKey}' failed type checker '{$typeCheckerStr}' on part '{$part}'. "
                    ." var_dump'ed value '{$dumped}' was returned. ");
            }
        }
    }

    static public function throwMismatchError($lookupKey,$lastPart,$part,$value) {

        ob_start();
            var_dump($value);
        $dumped = ob_get_clean();
        $dumped = substr($dumped,0,-1);
        throw new \Exception(
            "Lookup key '{$lookupKey}' is not valid. Failed for resource type '{$lastPart}' on key '{$part}' on var_dump'd value '{$dumped}'"
        );
    }


}

?>