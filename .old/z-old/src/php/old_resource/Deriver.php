<?php

namespace ROF\Resource;

class Deriver {
    

    static public function derive($resourceName,$allValues,$lookupKey){
        switch ($resourceName){
            case "file":
            case "image":
                $copy = $allValues;
                foreach ($copy as $index=>$fileInfo){
                    $allValues[$index] = static::deriveFileValues($fileInfo);
                }
                return $allValues;
            case "secure":
                $secureResource = new \ROF\Resource\Secured($allValues,$lookupKey);
                return $secureResource->rawValue();
            default:
                return $allValues;  
        }
    }

    static public function deriveFileValues($fileInfo){
        if (isset($fileInfo['path'])
            &&!isset($fileInfo['fileExtension'])){
            $fileInfo['fileExtension'] = pathinfo($fileInfo['path'],PATHINFO_EXTENSION);
        }
        
        return $fileInfo;
    }

}

?>