<?php


/**
 * @deprecated in favor of R_files(...$files);
 */
function R_init(...$files){
    \ROF\Resource::instance()->addFiles($files);
}

function R_files(...$files){
    \ROF\Resource::instance()->addFiles($files);
}


function R_all(){
    return \ROF\Resource::all();
}

function R($lookupKey='',$newValue=NULL){
    // var_dump(debug_backtrace());
    // return;
    // $callingFile = debug_backtrace()[1]['file'];
    // $callingFunction = debug_backtrace()[1]['function'];
    return \ROF\Resource::instance()->getResource($lookupKey,$newValue);
}

function RParse($text,$type='rof'){
    $resource = new \ROF\Resource();
    $parsed = $resource->parseString($text,$type);
    $resource->addParsed($parsed);
    return \ROF\Resource::instance()->parseString($text,$type);
}
function RParseFlat($text,$type='rof'){
    $token = new \ROF\Resource\ROFToken('beforeParam',$text);
    $data = $token->asArray();
    return $data;
    $resource = new \ROF\Resource();
    $parsed = $resource->parseString($text,$type);
    // $resource->addParsed($parsed);
    return $parsed;
}

function R_ns($namespaceName=NULL){
    $callingFile = debug_backtrace()[1]['file'];
    $callingFunction = debug_backtrace()[1]['function'] ?? 'no-func';
    return \ROF\Resource::instance()->setScopedNamespace($callingFile,$callingFunction,$namespaceName);
}